import numpy as np

"""
fonction auxiliaire de 'affichage'
"""
def get_number_rep(x) :
    if x == int(x) :
        return str(int(x))
    else :
        return "%.2f" % x
"""
fonction auxiliaire de 'affichage'
"""
def get_str_rep(x,max_size,i,first,print0) :
    str_x = get_number_rep(abs(x))
    n = len(str_x)
    if x == -1. :
        if i == first :
            return " "*(max_size)+"-x_"+str(i+1)
        else :
            return "-"+" "*(max_size)+"x_"+str(i+1)
    elif x == 1. :
        if i == first :
            return " "*(1+max_size)+"x_"+str(i+1)
        else :
            return "+"+" "*(max_size)+"x_"+str(i+1)
    elif x == 0.:
        if print0 :
            return " "*(max_size+3)+"0"
        else :
            return " "*(max_size+1+3)
    elif x < 0 :
        if i == first :
            return " "*(max_size-n)+"-"+str_x+"x_"+str(i+1)
        else :
            return "-"+" "*(max_size-n)+str_x+"x_"+str(i+1)
    elif i == first :
        return " "*(1+max_size-n)+str_x+"x_"+str(i+1)
    else :
        return "+"+" "*(max_size-n)+str_x+"x_"+str(i+1)

"""
fonction permettant l'affichage des systèmes linéaires
"""
def affichage(A,y) :
    res = ""
    max_size = 0
    first = [-1]*len(A)
    for i in range(len(A)) :
        for j in range(len(A[i])) :
            max_size = max(max_size,len(get_number_rep(A[i][j])))
            if first[i] ==-1 and A[i][j] != 0 :
                first[i] = j
    for i in range(len(A)) :
        res+="|"
        for j in range(len(A[i])) :
            res+=get_str_rep(A[i][j],max_size,j,first[i],first[i]==-1 and j == len(A[i])-1)
        res=res+" = "+(get_number_rep(y[i]))+"\n"
    print(res)

"""
fonction retournant le système représenté par A et y dans lequel les lignes i et k ont été permutées.
"""
def permutation(A,y,i,k) :
    B=A[i].copy()
    z=y[i].copy()

    A[i]=A[k]
    y[i]=y[k]

    A[k]=B
    y[k]=z
    return A,y

"""
fonction retournant le système représenté par A et y dans lequel la variable n°j a été supprimée des lignes
n°i+1 à n en utilisant la ligne n°i
"""
def elimination(A,y,i,j) :
    for ligne in range(i+1,len(A)):
        coef=A[ligne,j]/A[i,j]
        A[ligne]=A[ligne]-coef*A[i]
        y[ligne]=y[ligne]-coef*y[i]
    return A,y

def Gauss(A,y) :
    i,j=0,0
    m,p=A.shape
    while (i,j) != (m,p):
        affichage(A,y)
        k,l=get_nextpivot(A,i,j)
        if (k,l) != (m,p):
        	if k != i:
        		permutation(A,y,i,k)
        	j=l
        	elimination(A,y,i,j)
        	i,j=i+1,j+1
        else:
        	i,j=m,p
    B=np.transpose(A)

    return A,y

def transpose(A):
	return 0

def solveTriSup(A,y) :
    n,p=A.shape
    x=np.zeros(p)
    nb_eq=n
    nb_inc=p


    for line in range(n-1,-1,-1):
        k=first_non_zero(A[line])
        if k != -1:
            tmp=y[line]
            for coll in range(k+1,p,1):
                tmp-=A[line][coll]*x[coll]
            tmp=tmp/A[line][k]
            x[k]=tmp
        elif not (np.isclose(y[line],0)):
            print("Pas de solution")
            return np.zeros(p)
        else:
        	nb_eq=nb_eq-1
    if nb_eq==nb_inc:
    	print("Il existe une unique solution !")
    elif nb_eq==0:
    	print("Il existe une unique solution !")
    else:
    	print("Il existe une infinité de solution !") 
    return x

def first_non_zero(L) :
	for k in range(0,len(L)):
		if not (np.isclose(L[k],0)):
			return k
	return -1

def get_nextpivot(A,i,j) :
    m,n=A.shape
    for colum in range(j,n):
        for line in range(i,m):
            if not (np.isclose(A[line,colum],0)):
                return line,colum
    return m,n

A=np.array([[-1.,-3.,-2.],[1.,2.,3.],[2.,-2.,1.]])
y=np.array([3.,4.,4.])

# affichage(A,y)

# Gauss(A,y)

# affichage(A,y)


A,y = Gauss(A,y)

# systèmes demandés dans les exercices de TP
As=[]
ys=[]
As.append(np.zeros((2,2)))
ys.append(np.zeros(3))
As.append(np.array([[-1.,-3.,0],[1.,2.,3.],[2.,-2.,1.]]))
ys.append(np.array([3.,4.,4.]))
As.append(np.array([[-2.,10.,3.],[-5.,34.,12.],[-1.,8.,3.]]))
ys.append(np.array([-18.,-54.,-12.]))
As.append(np.array([[-3.,-1.,4.,3.],[0.,0.,4.,2.],[-2.,-2.,-2.,1.]]))
ys.append(np.array([8.,10.,-1.]))

As.append(np.array([[2.,15.,3.],[0.,21.,7.],[4.,9.,-1.]]))
ys.append(np.array([25.,35.,10.]))
As.append(np.array([[4.,3.,4.],[0.,0.,-1.],[3.,-3.,1.],[0,-1.,3.]]))
ys.append(np.array([1.,0.,6.,1.]))
As.append(np.array([[0.,0.,-2.],[-2.,2.,-2.],[-4.,4.,-4.],[-2.,2.,-4.]]))
ys.append(np.array([3.,-4.,-8.,-1.]))

# Question 13
As.append(np.array([[-1.,1.,1.],[1.,-1.,1.],[1.,1.,-1.]]))
ys.append(np.array([2.,-3.,-1.]))
As.append(np.array([[1.,2.,1.,1.],[2.,3.,3.,2.],[-1.,0.,-1.,1.],[-2.,-1.,0.,4.]]))
ys.append(np.array([7.,14.,-1.,2.]))

# Exemple
As.append(np.array([[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.]]))
ys.append(np.array([0.,0.,0.,0.]))

# Question 14
As.append(np.array([[2.,-1.,-5.],[3.,4.,-4.],[-13.,-21.,15.],[11.,11.,-17.]]))
ys.append(np.array([11.,4.,-9.,23.]))

for i in range(len(As)) :
    print("------- système n°"+str(i+1)+" -------------")
    affichage(As[i],ys[i])
    As[i],ys[i] = Gauss(As[i],ys[i])
    print(solveTriSup(As[i],ys[i]))

def estLiee(A):
	B=A.transpose()
	return 0
F1=1.*np.array([[7,28,21,3,-1],[1,4,3,-1,4],[-3,-12,-9,-7,17],[24,96,72,6,9],[7,28,21,-3,-3]])
y1 = np.zeros(5)
F2=1.*np.array([7,3,-1,1,-1],[4,7,-3,-3,0],[6,-1,2,2,1],[1,7,3,-3,7],[-3,4,3,4,0])
y2 = np.zeros(5)
F1,y = Gauss(F1,y1)
print(F1.transpose())